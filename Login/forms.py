'''
Author = Meet Shah
Date:    10/05/2018
Source:  This link helped me to write validation code
         https://stackoverflow.com/questions/2472422/django-file-upload-size-limit
'''

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
from django.conf import settings

class EditProfileForm(UserChangeForm):
    class Meta:
        model = User
        fields =(
            'first_name',
            'last_name',
            'email'
        )