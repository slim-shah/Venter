"""
Author = Meet Shah
After validating post request, controll will transfer to this class to read uploaded csv and append category column in it.
This code assumes that column number 4{ column 0 being base column} is always going to be complaint title.

source :  Read only headers from csv file
            => https://stackoverflow.com/questions/24962908/how-can-i-read-only-the-header-column-of-a-csv-file-using-python
"""
import pandas as pd
from django.conf import settings
import os
import operator

# from .ML_model.ClassificationService import ClassificationService
from Prediction.ML_model.model.ClassificationService import ClassificationService
from Prediction.ML_model.SpeakUp.Model.SpeakupClassificationService import ClassificationService_speakup


class CSV_FILE:
    filename = ''
    username = ''
    group = ''

    def __init__(self, file_name, user_name, company):
        self.filename = file_name
        self.username = user_name
        self.group = company

    def check_csvfile_header(self):
        PATH = os.path.join(settings.MEDIA_ROOT, self.username, "CSV", "input", self.filename)
        csvfile = pd.read_csv(PATH, nrows=1, encoding='utf-8').columns
        Company_Columns = []
        category_list = []
        if self.group == "ICMC":
            self.cs = ClassificationService()
            Company_Columns = settings.ICMC_HEADERS
            category_list =  settings.ICMC_CATEGORY_LIST

        elif self.group == "SpeakUP":
            self.cs = ClassificationService_speakup()
            Company_Columns = settings.SPEAKUP_HEADERS
            category_list = settings.SPEAKUP_CATEGORY_LIST

        if len(csvfile) == len(Company_Columns):
            for i in range(len(Company_Columns)):
                if Company_Columns[i].strip() == csvfile[i].strip():
                    continue
                else:
                    print("BLA" + Company_Columns[i] + "bla" + csvfile[i])

                    return False, []
            return True, category_list
        else:
            print(csvfile)
            print("Length of csv file: " + str(len(csvfile)))
            print("Length of prefix column: " + str(len(Company_Columns)))
            return False, []

    def delete(self):
        PATH = os.path.join(settings.MEDIA_ROOT, self.username, "CSV", "input", self.filename)
        os.remove(PATH)

    def write_file(self, correct_category):
        csvfile = pd.read_csv(os.path.join(settings.MEDIA_ROOT, self.username, "CSV", "input", self.filename), sep=',',
                              header=0)
        csvfile.insert(loc=0, column='Predicted_Category', value=correct_category)
        csvfile.to_csv(os.path.join(settings.MEDIA_ROOT, self.username, "CSV", "output", self.filename), sep=',',
                       encoding='utf-8', index=False)

    def Read_file(self):
        # encoding='utf-8' for mgcm
        # csvfile = pd.read_csv("./media/" +self.filename, sep=',', header=0)
        csvfile = pd.read_csv(settings.MEDIA_ROOT + "/" + self.username + "/CSV/input" + "/" + self.filename, sep=',',
                              header=0, encoding='utf-8')
        # print(csvfile.columns)

        Dict_List = []

        for row in csvfile.iterrows():
            dict = {}
            index, data = row
            # cats = {'category1':80, 'category2':10, 'category3':10}
            if self.group == "ICMC":
                complaint_title = data['complaint_title']
                complaint_description = data['complaint_description']
                dict['problem_description'] = complaint_description
                dict['index'] = index
                cats = self.cs.get_top_3_cats_with_prob(complaint_title)

            elif self.group == "SpeakUP":
                complaint_title = data['text']
                dict['problem_description'] = complaint_title
                dict['index'] = index
                cats = self.cs.get_top_3_cats_with_prob(complaint_title)
                #cats = {'category1': 0.08, 'category2': 0.01, 'category3': 0.01}

            for k in cats:
                cats[k] = int(cats[k]*100)

            sorted_cats = sorted(cats.items(), key= operator.itemgetter(1), reverse= True)
            dict['category'] = sorted_cats
            Dict_List.append(dict)

        del self.cs
        return Dict_List, csvfile.shape[0]
