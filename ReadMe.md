# Instruction to use Venter on Windows machine

## Pre-Requisites:
 Python version greater than or equal to 3. If you don't already have installed python on your system then go to this website and scroll to bottom of page and download Windows x86-64 executable installer.<br>
*  https://www.python.org/downloads/release/python-365/ 

**Note** :- Please don't forget to set python.exe on path variable. If you don't know how to set path variable then contact Venter team they will guide you.

## Next Step after installing python:

- Once you have installed python, then you will have to install all neccessary library to run venter in your system. You can download all libraries by double clicking 
	'install.exe'. 
	
- The location of that file will be "Venter\Install libraries\install.exe".

- After installing all libraries, download all nltk related data. You can do that by 
	clicking on "nltk_download.exe". 
- The location of nltk_download can be found on "Venter\Install libraries\nltk_download.exe". 
- The Once program starts to run a new window will pop up. In that window select 'all' option and click on download tab and wait for downlload to complete.

## Running Venter

- To start venter services, double click on "StartServer.exe". This will take sometime as it will start the Django server and load Machine Learning model into memory. 

- Once you see the message that server has started successfully, open your browser and type this address to use venter services  "localhost:8080/127.0.0.1". 